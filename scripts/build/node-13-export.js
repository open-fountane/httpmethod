const fs = require('fs')

const subRepositories = {
  'axios':"httpmethod-axios"
}

const snakeCaseToCamelCase = str =>
  str.replace(/([-_][a-z])/g, group => group.toUpperCase().replace('-', ''))

const copyCore = () => {
  fs.writeFileSync(
    `${process.cwd()}/dist/httpmethod.mjs`,
    fs.readFileSync(`${process.cwd()}/dist/httpmethod.module.js`)
  )
}

const copy = (source,name) => {
  const filename = name.includes('-') ? snakeCaseToCamelCase(name) : name
  fs.writeFileSync(
    `${process.cwd()}/${source}/dist/${filename}.mjs`,
    fs.readFileSync(`${process.cwd()}/${source}/dist/${filename}.module.js`)
  )
}

copyCore()
Object.keys(subRepositories).forEach((x)=>{
  copy(x,subRepositories[x])
})
