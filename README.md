# httpmethod

A simple SDK method builder based on templates similar to Loopback 3

## Usage

```js
const { createApp } = require('@fountane/httpmethod')
const { requestor: axiosRequestor } = require('@fountane/httpmethod/axios')

const app = createApp(axiosRequestor)

app.addMethod({
  method: 'GET',
  name: 'fetchTodos',
  url: 'https://jsonplaceholder.typicode.com/todos/#{{id}}',
})

// somewhere else in the codebase

const response = await app.fetchTodos({
  id,
})

response //=> axios response ({data,request,config,...etc})
```
