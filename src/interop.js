const variableRegex = variableName => new RegExp(`\#\{\{(${variableName})\}\}`)

export function interop(str, toReplace, val) {
  const interopRgx = variableRegex(toReplace)
  if (!variableRegex(toReplace).test(str)) return [false, str]
  return [true, str.replace(interopRgx, val)]
}
