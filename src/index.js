import { interop } from './interop.js'

function addMethod(requestor, methods, options) {
  const fn = function builder(values = {}) {
    let url = options.url
    options.method = options.method || 'GET'
    let query = options.query || {}
    let body = options.body || {}

    const sParams = new URLSearchParams()
    const data = {}

    Object.keys(values).forEach(key => {
      const interResult = interop(url, key, values[key])

      url = interResult[1]

      Object.keys(query).forEach(queryKey => {
        const [_, value] = interop(query[queryKey], key, values[key])
        !sParams.has(queryKey) && sParams.append(queryKey, value)
      })

      Object.keys(body).forEach(bodyKey => {
        const [_, value] = interop(body[bodyKey], key, values[key])
        data[bodyKey] = value
      })
    })

    if (Array.from(sParams).length > 0) {
      url += `?${sParams.toString()}`
    }

    return (async () => {
      return await requestor(url, options.method, data, options.options)
    })()
  }

  Object.defineProperty(methods, options.name, {
    get() {
      return fn
    },
  })
}

export function createApp(requestor) {
  const methods = {}
  methods.addMethod = options =>
    addMethod.apply(null, [requestor, methods, options])
  return methods
}
