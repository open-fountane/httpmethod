export declare function interop(
  str: string,
  toReplace: string,
  val: string
): [boolean, string]
