import axios from 'axios'

export async function requestor(url, method, data, options) {
  const response = await axios({
    method: method.toLowerCase(),
    url,
    data,
    // ...options,
  })
  return response
}
