import { AxiosInterceptorOptions } from 'axios'
declare type Method = 'GET' | 'POST' | 'PUT'
export declare function requestor(
  url: string,
  method: Method,
  data: Record<any, any>,
  options: AxiosInterceptorOptions
): Promise<import('axios').AxiosResponse<any, any>>
export {}
