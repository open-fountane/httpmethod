const expect = require('chai').expect
const { createApp } = require('../src')
const { requestor } = require('../axios/src')

describe('axios requestor', () => {
  it('basic url request', async () => {
    const app = createApp(requestor)
    const id = 1
    app.addMethod({
      method: 'GET',
      name: 'fetchTodos',
      url: 'https://jsonplaceholder.typicode.com/todos/#{{id}}',
    })

    const data = await app.fetchTodos({
      id,
    })

    expect(data.data.id).to.be.eq(id)
  })

  it('basic query request', async () => {
    const app = createApp(requestor)

    const id = 1

    const expectedUrl = `http://jsonplaceholder.typicode.com/comments?postId=${id}`

    app.addMethod({
      method: 'GET',
      name: 'fetchComments',
      url: 'http://jsonplaceholder.typicode.com/comments',
      query: {
        postId: '#{{id}}',
      },
    })

    const response = await app.fetchComments({
      id,
    })

    expect(response.config.url).to.be.eq(expectedUrl)
    expect(response.data.length).to.be.greaterThan(0)
    expect(response.data[0].postId).to.be.eq(1)
  })
})
