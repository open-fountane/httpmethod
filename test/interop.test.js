const expect = require('chai').expect
const { interop } = require('../src/interop')

describe('interop', () => {
  it('basic replacement', () => {
    const toReplace = '#{{h}}'
    const replaceWith = 'test1'
    const [isReplaced, value] = interop(toReplace, 'h', replaceWith)
    expect(value).to.be.equal(replaceWith)
    expect(isReplaced).to.be.true
  })

  it('basic non replacement', () => {
    const toReplace = 'no-replaceable-template'
    const replaceWith = 'test1'
    const [isReplaced, value] = interop(toReplace, '', replaceWith)
    expect(value).not.to.be.equal(replaceWith)
    expect(isReplaced).to.be.false
  })

  it('url replacement #1', () => {
    const toReplace = 'https://url.com/#{{property}}'
    const replaceWith = '1'
    const expectedResult = 'https://url.com/1'

    const [isReplaced, value] = interop(toReplace, 'property', replaceWith)

    expect(value).to.be.equal(expectedResult)
    expect(isReplaced).to.be.true
  })

  it('url replacement #2', () => {
    const toReplace = 'https://url.com/#{{property}}/'
    const replaceWith = '1'
    const expectedResult = 'https://url.com/1/'

    const [isReplaced, value] = interop(toReplace, 'property', replaceWith)

    expect(value).to.be.equal(expectedResult)
    expect(isReplaced).to.be.true
  })
})
